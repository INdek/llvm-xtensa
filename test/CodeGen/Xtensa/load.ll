; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @load32_imm0(i32* %a) {
; CHECK: load32_imm0:
; CHECK: l32i a2, a2, 0
  %b = load i32, i32* %a, align 4
  ret i32 %b
}

; TODO: Enable this
; define i32 @load32_imm1020(i32* %a) {
; ; _CHECK: load32_imm1020:
; ; _CHECK: l32i a2, a2, 1020
;   %b = getelementptr i32, i32* %a, i32 255
;   %c = load i32, i32* %b, align 4
;   ret i32 %c
; }
