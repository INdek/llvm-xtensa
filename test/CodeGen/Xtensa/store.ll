; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define void @store32_imm0(i32* %a) {
; CHECK: store32_imm0:
; CHECK: s32i a3, a2, 0
  store i32 0, i32* %a, align 4
  ret void
}

; TODO: Enable this
; define void @store32_imm1020(i32* %a) {
; ; _CHECK: store32_imm1020:
; ; _CHECK: s32i a3, a2, 1020
;   %b = getelementptr i32, i32* %a, i32 255
;   store i32 0, i32* %b, align 4
;   ret void
; }
