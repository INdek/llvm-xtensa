; RUN: llc < %s -O0 -march=xtensa 2>&1 | FileCheck %s

define void @branch_eq_jmp(i32 %a, i32 %b) {
; CHECK-LABEL: branch_eq_jmp:
; CHECK:        beq a3, a2, .{{[_A-Z0-9]+}}
; CHECK-NEXT:   j .{{[_A-Z0-9]+}}

  %tst1 = icmp eq i32 %b, %a
  br i1 %tst1, label %end, label %test12

test12:
  br label %end

end:
  ret void
}

