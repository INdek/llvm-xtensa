; TODO: Remove the mcpu=allfeatures
; RUN: llc < %s -O0 -march=xtensa -mattr=dense 2>&1 | FileCheck %s
; XFAIL: *

define i32 @add32_reg_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: add32_reg_reg:
; CHECK:   add.n a2, a2, a3
  %result = add i32 %a, %b
  ret i32 %result
}

define i32 @addi32_reg_imm_neg(i32 %a) nounwind {
; CHECK-LABEL: addi32_reg_imm_neg:
; CHECK:   addi.n a2, a2, -1
  %result = add i32 %a, -1
  ret i32 %result
}

define i32 @addi32_reg_imm_pos(i32 %a) nounwind {
; CHECK-LABEL: addi32_reg_imm_pos:
; CHECK:   addi.n a2, a2, 15
  %result = add i32 %a, 15
  ret i32 %result
}


define i32 @movn_arg2(i32 %a, i32 %b, i32 %c) nounwind {
; CHECK-LABEL: movn_arg2
; CHECK:   mov.n a2, a4
  ret i32 %c
}

