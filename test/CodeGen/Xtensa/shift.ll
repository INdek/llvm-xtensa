; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @shl_1(i32 %a) nounwind {
; CHECK-LABEL: shl_1:
; CHECK:   slli a2, a2, 1
  %result = shl i32 %a, 1
  ret i32 %result
}

define i32 @shl_31(i32 %a) nounwind {
; CHECK-LABEL: shl_31:
; CHECK:   slli a2, a2, 31
  %result = shl i32 %a, 31
  ret i32 %result
}


define i32 @shr_arith_1(i32 %a) nounwind {
; CHECK-LABEL: shr_arith_1:
; CHECK:   srai a2, a2, 1
  %result = ashr i32 %a, 1
  ret i32 %result
}

define i32 @shr_arith_31(i32 %a) nounwind {
; CHECK-LABEL: shr_arith_31:
; CHECK:   srai a2, a2, 31
  %result = ashr i32 %a, 31
  ret i32 %result
}




define i32 @shr_logic_1(i32 %a) nounwind {
; CHECK-LABEL: shr_logic_1:
; CHECK:   srli a2, a2, 1
  %result = lshr i32 %a, 1
  ret i32 %result
}

define i32 @shr_logic_31(i32 %a) nounwind {
; CHECK-LABEL: shr_logic_31:
; CHECK:   srli a2, a2, 31
  %result = lshr i32 %a, 31
  ret i32 %result
}

