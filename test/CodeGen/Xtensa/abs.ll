; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s
; XFAIL: *

define i32 @test_abs_i32(i32) {
; CHECK-LABEL: test_i32:
; CHECK:   abs a2, a2
  %2 = icmp slt i32 %0, 0
  %3 = sub nsw i32 0, %0
  %4 = select i1 %2, i32 %3, i32 %0
  ret i32 %4
}
