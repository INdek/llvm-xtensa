; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @or32_reg_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: or32_reg_reg:
; CHECK:   or a2, a2, a3
  %result = or i32 %a, %b
  ret i32 %result
}
