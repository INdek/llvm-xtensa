; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @sub32_reg_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: sub32_reg_reg:
; CHECK:   sub a2, a2, a3
  %c = sub i32 %a, %b
  ret i32 %c
}

define i32 @subx2(i32, i32) {
; CHECK-LABEL: subx2:
; CHECK:   subx2 a2, a2, a3
  %3 = shl i32 %0, 1
  %4 = sub i32 %3, %1
  ret i32 %4
}

define i32 @subx4(i32, i32) {
; CHECK-LABEL: subx4:
; CHECK:   subx4 a2, a2, a3
  %3 = shl i32 %0, 2
  %4 = sub i32 %3, %1
  ret i32 %4
}

define i32 @subx8(i32, i32) {
; CHECK-LABEL: subx8:
; CHECK:   subx8 a2, a2, a3
  %3 = shl i32 %0, 3
  %4 = sub i32 %3, %1
  ret i32 %4
}
