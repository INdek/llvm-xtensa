; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @zero() nounwind {
; CHECK-LABEL: zero:
; CHECK:   movi a2, 0
  ret i32 0
}

define i32 @pos_small() nounwind {
; CHECK-LABEL: pos_small:
; CHECK:   movi a2, 2047
  ret i32 2047
}

define i32 @neg_small() nounwind {
; CHECK-LABEL: neg_small:
; CHECK:   movi a2, -2048
  ret i32 -2048
}
