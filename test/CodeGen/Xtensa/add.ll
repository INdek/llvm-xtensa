; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

; TODO: add ADDMI tests once ADDI is fixed

define i32 @add32_reg_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: add32_reg_reg:
; CHECK:   add a2, a2, a3
  %result = add i32 %a, %b
  ret i32 %result
}


define i32 @add32_reg_imm_neg(i32 %a) nounwind {
; CHECK-LABEL: add32_reg_imm_neg:
; CHECK:   addi a2, a2, -128
  %result = add i32 %a, -128
  ret i32 %result
}

define i32 @add32_reg_imm_pos(i32 %a) nounwind {
; CHECK-LABEL: add32_reg_imm_pos:
; CHECK:   addi a2, a2, 127
  %result = add i32 %a, 127
  ret i32 %result
}



define i32 @addx2(i32, i32) {
; CHECK-LABEL: addx2:
; CHECK:   addx2 a2, a2, a3
  %3 = shl i32 %0, 1
  %4 = add i32 %3, %1
  ret i32 %4
}

define i32 @addx4(i32, i32) {
; CHECK-LABEL: addx4:
; CHECK:   addx4 a2, a2, a3
  %3 = shl i32 %0, 2
  %4 = add i32 %3, %1
  ret i32 %4
}

define i32 @addx8(i32, i32) {
; CHECK-LABEL: addx8:
; CHECK:   addx8 a2, a2, a3
  %3 = shl i32 %0, 3
  %4 = add i32 %3, %1
  ret i32 %4
}
