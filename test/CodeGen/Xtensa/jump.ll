; RUN: llc < %s -O0 -march=xtensa 2>&1 | FileCheck %s

define void @test_jump() {
; CHECK: test_jump:
; CHECK: j       .LBB0_1
; CHECK: j       .LBB0_1
  br label %1

; <label>:1:
  br label %1

  ret void
}
