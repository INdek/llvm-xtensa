; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @xor32_reg_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: xor32_reg_reg:
; CHECK:   xor a2, a2, a3
  %result = xor i32 %a, %b
  ret i32 %result
}
