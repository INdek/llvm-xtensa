; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define void @pei_4() {
; CHECK: pei_4:
; CHECK: addi    sp, sp, -4
; CHECK: addi    sp, sp, 4

  %r = alloca i32, align 4
  ret void
}


define void @pei_4096() {
; CHECK: pei_4096:
; CHECK: addmi    sp, sp, -4096
; CHECK: addmi    sp, sp, 4096

  %r = alloca i32, align 4096
  ret void
}

define void @pei_65536() {
; CHECK: pei_65536:
; CHECK: addmi   sp, sp, -32768
; CHECK: addmi   sp, sp, -32768
; CHECK: addmi   sp, sp, 32512
; CHECK: addmi   sp, sp, 32512
; CHECK: addmi   sp, sp, 512

  %r = alloca i32, align 65536
  ret void
}
