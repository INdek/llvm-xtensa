; RUN: llc < %s -march=xtensa 2>&1 | FileCheck %s

define i32 @neg32_reg(i32 %a) nounwind {
; CHECK-LABEL: neg32_reg:
; CHECK:   neg a2, a2
  %result = sub i32 0, %a
  ret i32 %result
}
