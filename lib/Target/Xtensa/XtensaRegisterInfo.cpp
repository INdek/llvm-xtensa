//===-- XtensaRegisterInfo.cpp - Xtensa Register Information ----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the Xtensa implementation of the MRegisterInfo class.
//
//===----------------------------------------------------------------------===//


#include "XtensaRegisterInfo.h"
#include "Xtensa.h"
#include "XtensaFrameLowering.h"
#include "XtensaMachineFunctionInfo.h"
#include "XtensaTargetMachine.h"
#include "XtensaInstrInfo.h"
#include "XtensaMachineFunctionInfo.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#define GET_REGINFO_TARGET_DESC
#include "XtensaGenRegisterInfo.inc"

using namespace llvm;

XtensaRegisterInfo::XtensaRegisterInfo() : XtensaGenRegisterInfo(Xtensa::A0) {}

const uint16_t *
XtensaRegisterInfo::getCalleeSavedRegs(const MachineFunction *MF) const {
  // TODO: Verify the calling convention
  return CALL0_CC_Save_SaveList;
}

BitVector XtensaRegisterInfo::getReservedRegs(const MachineFunction &MF) const {
  // TODO: Verify the calling convention
  BitVector Reserved(getNumRegs());

  Reserved.set(Xtensa::A0);
  Reserved.set(Xtensa::A1);
  Reserved.set(Xtensa::A8);
  return Reserved;
}

const uint32_t *XtensaRegisterInfo::getCallPreservedMask(const MachineFunction &MF,
                                                      CallingConv::ID) const {
  // TODO: Verify the calling convention
  return CALL0_CC_Save_RegMask;
}

bool XtensaRegisterInfo::requiresRegisterScavenging(const MachineFunction &MF) const {
  return true;
}

bool XtensaRegisterInfo::trackLivenessAfterRegAlloc(const MachineFunction &MF) const {
  return true;
}

bool XtensaRegisterInfo::useFPForScavengingIndex(const MachineFunction &MF) const {
  assert(0 && "Not implemented yet!");
  //return false;
}

void XtensaRegisterInfo::eliminateFrameIndex(MachineBasicBlock::iterator II,
                                          int SPAdj, unsigned FIOperandNum,
                                          RegScavenger *RS) const {
  assert(SPAdj == 0 && "Unexpected non-zero SPAdj value");

  MachineInstr &MI = *II;
  MachineFunction &MF = *MI.getParent()->getParent();
  // MachineRegisterInfo &MRI = MF.getRegInfo();
  const XtensaInstrInfo *TII = MF.getSubtarget<XtensaSubtarget>().getInstrInfo();
  DebugLoc DL = MI.getDebugLoc();

  int FrameIndex = MI.getOperand(FIOperandNum).getIndex();
  unsigned BasePtrReg = getFrameRegister(MF);

  assert(!MI.isDebugValue() && "DBG_VALUEs should be handled in target-independent code");
  assert( (
        MI.getOpcode() == Xtensa::S32I ||
        MI.getOpcode() == Xtensa::L32I ||
        MI.getOpcode() == Xtensa::FRMIDX
  ) && "Unsupported opcode when eliminating frame index");


  int Offset =
    MF.getFrameInfo().getObjectOffset(FrameIndex) +
    MI.getOperand(FIOperandNum + 1).getImm(); // Add the incoming offset.

  assert(Offset % 4 == 0  && "Unaligned frame index offsets are not yet supported");


  II->getParent()->dump();

  // This is actually "load effective address" of the stack slot instruction.
  if (MI.getOpcode() == Xtensa::FRMIDX) {
    // TODO: There is some inneficiency here, ISEL sometimes poorly choses to emit
    // a FRMIDX even if we could handle the immediate offset in the L32I/S32I
    // instruction.

    assert(Offset >= -128 && Offset <= 127 && "FRMIDX offset not handled");
    errs() << "Handling FRMIDX" << "\n";

    MI.setDesc(TII->get(Xtensa::ADDI));
  }

  //assert(Offset >= 0 && Offset <= 1020 && "Frame index offsets over 1020 are not supported");

  MI.getOperand(FIOperandNum).ChangeToRegister(BasePtrReg, false);
  // We need a negative offset because we set the stackpointer after the variables
  MI.getOperand(FIOperandNum + 1).ChangeToImmediate(-Offset);
}

unsigned XtensaRegisterInfo::getFrameRegister(const MachineFunction &MF) const {
  //unsigned BasePtr = (TFI->hasFP(MF) ? Xtensa::FP : Xtensa::SP);
  return Xtensa::A1;
}
