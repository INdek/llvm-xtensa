//===-- XtensaFrameLowering.cpp - Frame info for Xtensa Target --------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains Xtensa frame information that doesn't fit anywhere else
// cleanly...
//
//===----------------------------------------------------------------------===//

#include "XtensaFrameLowering.h"
#include "Xtensa.h"
#include "XtensaInstrInfo.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineModuleInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/RegisterScavenging.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetOptions.h"
#include <algorithm> // std::sort

using namespace llvm;

//===----------------------------------------------------------------------===//
// XtensaFrameLowering:
//===----------------------------------------------------------------------===//
XtensaFrameLowering::XtensaFrameLowering()
    : TargetFrameLowering(TargetFrameLowering::StackGrowsDown, 4, 0) {
  // Do nothing
}

bool XtensaFrameLowering::hasFP(const MachineFunction &MF) const {
  return MF.getTarget().Options.DisableFramePointerElim(MF) ||
         MF.getFrameInfo().hasVarSizedObjects();
}

uint64_t XtensaFrameLowering::computeStackSize(MachineFunction &MF) const {
  MachineFrameInfo MFI = MF.getFrameInfo();
  uint64_t StackSize = MFI.getStackSize();
  /*
   * TODO rounduptoalignment
  unsigned StackAlign = getStackAlignment();
  if (StackAlign > 0) {
    StackSize = RoundUpToAlignment(StackSize, StackAlign);
  }
  */
  return StackSize;
}

/// Adjusts a register without allocating any other temporary registers
/// We do this by subtracting the biggest value succesivly until we reach 0
/// There probably are better algorithms for doing this
static void adjustRegNoTmp(MachineBasicBlock &MBB, MachineBasicBlock::iterator MBBI,
                   const DebugLoc &DL, const TargetInstrInfo &TII,
                   unsigned DstReg, int Val, unsigned MIFlags) {
  while (Val) {
    if (Val / 256 != 0) {
      int UpdateSize = (Val / 256) * 256; // This aligns the update

      UpdateSize = (UpdateSize < 0) ?
        std::max(-32768, UpdateSize) :
        std::min( 32512, UpdateSize);

      BuildMI(MBB, MBBI, DL, TII.get(Xtensa::ADDMI), DstReg)
        .addReg(DstReg)
        .addImm(UpdateSize)
        .setMIFlags(MIFlags);

      Val -= UpdateSize;
      continue;
    }


    if (Val > -256 && Val < 256) {
      int UpdateSize = (Val < 0) ?
        std::max(-128, Val) :
        std::min( 127, Val);

      BuildMI(MBB, MBBI, DL, TII.get(Xtensa::ADDI), DstReg)
        .addReg(DstReg)
        .addImm(UpdateSize)
        .setMIFlags(MIFlags);

      Val -= UpdateSize;
      continue;
    }
  }
}

// TODO: we could use this implementation for a function like: incrementRegister
static void emitSPUpdate(MachineBasicBlock &MBB,
                        MachineBasicBlock::iterator &MBBI, const DebugLoc &DL,
                        const TargetInstrInfo &TII, int NumBytes,
                        unsigned MIFlags = MachineInstr::NoFlags) {
  MachineFunction *MF = MBB.getParent();
  CallingConv::ID CallConv = MF->getFunction().getCallingConv();

  if (!NumBytes) {
    return;
  }

  assert(CallConv != CallingConv::Xtensa_Windowed &&
      "EmitSpUpdate cannot yet handle the Windowed Calling convention");

  errs() << "EmitSPUpdate of: " << NumBytes << "\n";

  // We should use the MOVSP instruction if the calling convetion is
  // the Windowed CC, but we haven't implemented that yet, so a simple
  // ADDI should suffice

  adjustRegNoTmp(MBB, MBBI, DL, TII, Xtensa::A1, NumBytes, MIFlags);

  return;
}

// TODO: Support CALL4/8/12 and ENTRY instructions
// TODO: Add tests for this!
void XtensaFrameLowering::emitPrologue(MachineFunction &MF,
                                    MachineBasicBlock &MBB) const {
  // Compute the stack size, to determine if we need a prologue at all.
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  MachineBasicBlock::iterator MBBI = MBB.begin();
  DebugLoc DL = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();

  assert(!hasFP(MF) && "Frame pointer not yet supported");

  uint64_t StackSize = computeStackSize(MF);

  // If we don't need to move the stack, early exit
  if (!StackSize) {
    return;
  }

  emitSPUpdate(MBB, MBBI, DL, TII, -StackSize, MachineInstr::FrameSetup);
}

void XtensaFrameLowering::emitEpilogue(MachineFunction &MF,
                                       MachineBasicBlock &MBB) const {
  // Compute the stack size, to determine if we need an epilogue
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  MachineBasicBlock::iterator MBBI = MBB.getLastNonDebugInstr();
  DebugLoc DL = MBBI->getDebugLoc();

  assert(!hasFP(MF) && "Frame pointer not yet supported");

  uint64_t StackSize = computeStackSize(MF);

  // If we don't need to move the stack, early exit
  if (!StackSize) {
    return;
  }

  emitSPUpdate(MBB, MBBI, DL, TII, StackSize, MachineInstr::FrameDestroy);
}

// This function eliminates ADJCALLSTACKDOWN, ADJCALLSTACKUP pseudo
// instructions
MachineBasicBlock::iterator XtensaFrameLowering::eliminateCallFramePseudoInstr(
    MachineFunction &MF, MachineBasicBlock &MBB,
    MachineBasicBlock::iterator I) const {
  /*
  if (I->getOpcode() == Xtensa::ADJCALLSTACKUP ||
      I->getOpcode() == Xtensa::ADJCALLSTACKDOWN) {
    MBB.erase(I);
  }
  */
}
